#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <errno.h>

enum { BUF_SIZE = 1024 };

int main()
{
  int fd  = open("testfile_str", O_RDWR);
  if (fd == -1)
  {
    fprintf(stderr, "open error: %s\n", strerror(errno));
    exit(-1);
  }

  char template[] = "/tmp/mkstemp_example_XXXXXX";
  int fd2 = mkstemp(template);
  printf("Generated filename was: %s\n", template);
  if (fd2 == -1)
  {
    fprintf(stderr, "mkstemp error: %s\n", strerror(errno));
    exit(-1);
  }
    
 // unlink(template);

  int N = 10;
  char buffer[BUF_SIZE];
  char nline = '\n';
  ssize_t len = 0;
  size_t pos = 0;
  while((len = read(fd, buffer, BUF_SIZE)) > 0)
  {
    if(len > N)
    {
      while (pos < (size_t) len)
      {
        pos += write(fd2, buffer + pos, N);
        write(fd2, &nline, 1);
      }
      pos = 0;
    }
    else
    {
      write(fd2, buffer, len);
    }
  }
  
  ftruncate(fd, 0);
  lseek(fd, 0, SEEK_SET);
  lseek(fd2, 0, SEEK_SET);
  while((len = read(fd2, buffer, BUF_SIZE)) > 0)
  {
    if(write(fd, buffer, len) < len)
    {
      fprintf(stderr, "write back error: %s\n", strerror(errno));
      exit(-1);
    }
  }

  close(fd);
  close(fd2);


  return 0;
}
