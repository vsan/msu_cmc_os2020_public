#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>


/*
  по-хорошему должны быть ещё проверки на каждый системный вызов
*/

int main()
{
  struct stat sb;
  int fd  = open("file.txt", O_RDONLY);
  fstat(fd, &sb);

  int fd2 = open("file2.txt", O_WRONLY | O_CREAT | O_TRUNC, (sb.st_mode & ~S_IFMT));

  int N = 64;
  char buffer[1024];
  char nline = '\n';
  ssize_t len = 0;
  size_t pos = 0;
  while((len = read(fd, buffer, 1024)) > 0)
  {
    if(len > N)
    {
      while (pos < (size_t) len)
      {
        pos += write(fd2, buffer + pos, N);
        write(fd2, &nline, 1);
      }
      pos = 0;
    }
    else
    {
      write(fd2, buffer, len);
    }
  }

  close(fd);
  close(fd2);


  return 0;
}
