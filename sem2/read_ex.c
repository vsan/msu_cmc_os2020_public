#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

enum { MAX_READ = 20 };

int main (void)
{
  char buffer[MAX_READ];
  ssize_t nRead;
  
  nRead = read(STDIN_FILENO, buffer, MAX_READ);
  if (nRead == -1)
  { 
    fprintf(stderr, "error reading from STDIN : %s\n", strerror(errno));
    exit(1);
  }
  
  printf("Read %ld bytes: %s\n", nRead, buffer);
  
  exit(0);
}
