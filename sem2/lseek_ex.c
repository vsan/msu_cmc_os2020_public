#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ctype.h>


long parseLong(char* str, int base)
{
  char *end;
  long res = strtol(str, &end, base);
  
  if(end == str)
  {
     fprintf(stderr, "parseLong: no digits found:\n");
     return 0;
  }
  
  if(end == str)
  {
     fprintf(stderr, "parseLong: no digits found:\n");
     return 0;
  }
  
  return res;
}

int main(int argc, char* argv[])
{
  size_t len;
  off_t offset;
  int fd, ap, j;
  char *buf;
  ssize_t nRead, nWritten;
  
  if (argc < 3 || strcmp(argv[1], "--help") == 0)
  {
    fprintf(stderr, "Wrong arguments. Example usage:\n");
    fprintf(stderr, "%s file {r<length>|R<length>|w<string>|s<offset>}...\n", argv[0]);
    exit(1);
  }

  // Права доступа: rw-rw-rw-
  fd = open(argv[1], O_RDWR | O_CREAT, S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH);

  if (fd == -1)
  {
    fprintf(stderr, "error opening %s : %s\n", argv[1], strerror(errno));
    exit(1);
  }
  
  for (ap = 2; ap < argc; ap++) 
  {
    switch (argv[ap][0]) 
    {
    case 'x':
    case 'r':
      len = parseLong(&argv[ap][1], 10);
      buf = malloc(len);
      if (buf == NULL)
      {
        fprintf(stderr, "malloc error\n");
      }
      
      nRead = read(fd, buf, len);
      if (nRead == -1)
      {
        fprintf(stderr, "error reading from file: %s\n", strerror(errno));
      }

      if (nRead == 0) 
      {
        printf("%s: Nothing to read\n", argv[ap]);
      } 
      else 
      {
        printf("%s: ", argv[ap]);
        for (j = 0; j < nRead; j++) 
        {
          if (argv[ap][0] == 'r')
            printf("%c", isprint((unsigned char)buf[j]) ? buf[j] : '?');
          else
            printf("%02x ", (unsigned int) buf[j]);
        }
        printf("\n");
      }
      free(buf);
      break;
      
    case 'w':
      nWritten = write(fd, &argv[ap][1], strlen(&argv[ap][1]));
      if (nWritten == -1)
      {
        fprintf(stderr, "error writing to file: %s\n", strerror(errno));
      }
      
      printf("%s: wrote %ld bytes\n", argv[ap], (long) nWritten);
      break;
      
    case 's':
      offset = parseLong(&argv[ap][1], 10);
      if (lseek(fd, offset, SEEK_SET) == -1)
      {
        fprintf(stderr, "lseek error: %s\n", strerror(errno));
      }
      
      printf("%s: successfully set file position\n", argv[ap]);
      break;
      
    default:
      fprintf(stderr, "error parsing argument: %s\n", argv[ap]);
    }
  }
  
  exit(EXIT_SUCCESS);
}
