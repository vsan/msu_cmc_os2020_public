#include <unistd.h>
#include <stdlib.h>

int main()
{
  char c;
  while (read(0, &c, 1) == 1) 
  {
    if(write(1, &c, 1) != 1)
      exit(1);
  }
}
