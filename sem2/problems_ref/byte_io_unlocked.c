#include <stdio.h>

int main()
{
  int c;
  while ((c = getchar_unlocked()) != EOF) 
  {
    putchar_unlocked(c);
  }
  
  return 0;
}
