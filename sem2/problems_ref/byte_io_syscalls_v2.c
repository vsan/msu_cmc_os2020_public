#include <unistd.h>
#include <stdlib.h>


enum { BUF_SIZE = 128 };

int main()
{
  char c[BUF_SIZE];
  int nRead = 0;
  while ((nRead = read(0, &c, BUF_SIZE)) > 0) 
  {
    for(int i = 0; i < nRead; ++i)
    {
      if(write(1, &c[i], 1) != 1)
        exit(1);
    }
  }
}
