#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>

enum { BUF_SIZE = 64 };

int main(void)
{
  int fd;
  char buf[BUF_SIZE];
  
  // Открыть файл только для чтения
  fd = open("open_ex.c", O_RDONLY);
  if (fd == -1)
  {
    fprintf(stderr, "error opening \"open_ex.c\" : %s\n", strerror(errno));
    exit(1);
  }
  
  int nRead = read(fd, buf, BUF_SIZE);
  if(nRead == -1)
  {
    fprintf(stderr, "error reading from \"open_ex.c\" : %s\n", strerror(errno));
    exit(1);
  }

  int nWritten = write(STDOUT_FILENO, buf, nRead);
  if(nWritten == -1 || nWritten != nRead)
  {
    fprintf(stderr, "error writing : %s\n", strerror(errno));
    exit(1);
  }
  fputc('\n', stdout);
  
  if (close(fd) == -1)
  {
    fprintf(stderr, "error closing file:  %s\n", strerror(errno));
    exit(1);  
  }
  
  /* ************************************ */
  
  // Открыть новый или существующий файл для чтения или записи с усечением до 0 байт.
  // Права доступа - чтение и запись только для владельца.
  fd = open("myfile", O_RDWR | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
  if (fd == -1)
  {
    fprintf(stderr, "error opening \"myfile\" : %s\n", strerror(errno));
    exit(1);
  }
  
  nWritten = write(fd, buf, nRead);
  if(nWritten == -1 || nWritten != nRead)
  {
    fprintf(stderr, "error writing : %s\n", strerror(errno));
    exit(1);
  }
  
  if (close(fd) == -1)
  {
    fprintf(stderr, "error closing file:  %s\n", strerror(errno));
    exit(1);  
  }
  
  /* ************************************ */
  
  // Открыть новый или существующий файл для записи. Запись происходит всегда в конец файла.
  fd = open("open_ex.log", O_WRONLY | O_CREAT | O_APPEND, S_IRUSR | S_IWUSR);
  if (fd == -1)
  {
    fprintf(stderr, "error opening \"open_ex.log\" : %s\n", strerror(errno));
    exit(1);
  }
  
  char msg[] = "successful open_ex run!\n";
  
  nWritten = write(fd, msg, sizeof(msg));
  if(nWritten != sizeof(msg))
  {
    fprintf(stderr, "error writing log: %s\n", strerror(errno));
    exit(1);
  }
  
  if (close(fd) == -1)
  {
    fprintf(stderr, "error closing file:  %s\n", strerror(errno));
    exit(1);  
  }

  // Пробуем открыть существующий файл "myfile" с флагом O_EXCL. Ожидаем получить ошибку.
  fd = open("myfile", O_RDWR | O_CREAT | O_EXCL, S_IRUSR | S_IWUSR);
  if (fd == -1)
  {
    fprintf(stderr, "error opening \"myfile\" : %s\n", strerror(errno));
    exit(1);
  }

  return 0;
}
