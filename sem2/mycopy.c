#include <sys/types.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <fcntl.h>


enum { BUF_SIZE = 1024 };


int main(int argc, char* argv[])
{
  int inFile, outFile, flags;
  mode_t permissions;
  ssize_t nRead;
  char buf[BUF_SIZE];

  if (argc != 3 || strcmp(argv[1], "--help") == 0)
  {
    fprintf(stderr, "Wrong arguments. Example usage:\n  %s src-file dest-file\n", argv[0]);
    return 1;
  }
  
  inFile = open(argv[1], O_RDONLY);
  if (inFile == -1)
  {
    fprintf(stderr, "error opening %s : %s\n", argv[1], strerror(errno));
    return 1;
  }
    
  flags = O_CREAT | O_WRONLY | O_TRUNC;
  permissions = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP | S_IROTH | S_IWOTH; // rw-rw-rw-
  outFile = open(argv[2], flags, permissions);
  if (outFile == -1)
  {
    fprintf(stderr, "error opening %s : %s\n", argv[2], strerror(errno));
    return 1;
  }
  
  while ((nRead = read(inFile, buf, BUF_SIZE)) > 0)
  {
    if (write(outFile, buf, nRead) != nRead)
    {
      fprintf(stderr, "write: couldn't write whole buffer\n");
      return 1;
    }
  }
  
  if (nRead == -1)
  {
    fprintf(stderr, "error reading:  %s\n", strerror(errno));
    return 1;
  }
  
  if (close(inFile) == -1)
  {
    fprintf(stderr, "error on close for file %s:  %s\n", argv[1], strerror(errno));
    return 1;  
  }
  
  if (close(outFile) == -1)
  {
    fprintf(stderr, "error on close for file %s:  %s\n", argv[2], strerror(errno));
    return 1;
  }
  
  return 0;
}
