#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

/*
gcc -Wall -O2 -g dlopen_ex.c -o dlopen_ex -ldl
*/

int main()
{
    void *handle = dlopen("./libmyso.so", RTLD_LAZY);
    if (!handle)
    {
      fprintf(stderr, "dlopen error: %s\n", dlerror());
      exit(1);
    }

    void *func = dlsym(handle, "myfunc");
    if (!func)
    {
      fprintf(stderr, "dlsym error: %s\n", dlerror());
      exit(1);
    }
    
    void *func2 = dlsym(handle, "myprint");
    if (!func2)
    {
      fprintf(stderr, "dlsym error: %s\n", dlerror());
      exit(1);
    }

    int x;
    while (scanf("%d", &x) == 1) 
    {
        int y = ((int (*)(int)) func)(x);
        ((void (*) (int, char*)) func2)(x, "x");
        ((void (*) (int, char*)) func2)(y, "y");
    }
}
