/*
gcc -g -Wall -O2 -fPIC -DPIC -shared myso_ex.c -o libmyso.so
*/
#include <stdio.h>

int myfunc(int x)
{
  return x + 1;
}

void myprint(int x, char* name)
{
  printf("%s = %d\n", name, x);
}
