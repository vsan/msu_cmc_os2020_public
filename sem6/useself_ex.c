#include <dlfcn.h>
#include <stdio.h>
#include <stdlib.h>

/*
Для компиляции потребуется опция -rdynamic
Программа ищет функцию по имени в самой себе.

gcc -g -Wall -O2 -Wl,--export-dynamic useself_ex.c -o useself_ex -ldl
*/

int myfunc(int x)
{
    return x * 2;
}

int main()
{
    void *handle = dlopen(NULL, RTLD_LAZY);
    if (!handle)
    {
      fprintf(stderr, "dlopen error: %s\n", dlerror());
      exit(1);
    }

    void *func = dlsym(handle, "myfunc");
    if (!func)
    {
      fprintf(stderr, "dlsym error: %s\n", dlerror());
      exit(1);
    }

    int x;
    while (scanf("%d", &x) == 1) 
    {
        int y = ((int (*)(int)) func)(x);
        printf("%d * 2 = %d\n", x, y);
    }
    dlclose(handle);
}
