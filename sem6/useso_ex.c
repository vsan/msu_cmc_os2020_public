#include <stdio.h>
#include <stdlib.h>

/*
gcc -g -Wall -Wl,-rpath,. -o useso_ex useso_ex.c -L. -lmyso
*/

int main()
{
    int x;
    while (scanf("%d", &x) == 1) 
    {
        int y = myfunc(x);
        myprint(x, "x");
        myprint(y, "y");
    }
}
