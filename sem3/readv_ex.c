#include <sys/uio.h>
#include <unistd.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

enum { STR_SIZE = 10};

struct point
{
  int x;
  int y;
};

int
main(int argc, char *argv[])
{
  const char* filename = "test_file_readv";
  int fd;
  struct iovec iov[4];
  
  struct point myStruct = {.x = 1, .y = 2}; // буфер 1
  float x = 3.14f; // буфер 2
  char str[] = "scatter-gather i/o"; // буфер 3
  int a = 100;  // буфер 4
  ssize_t numRead, numWritten, totRequired;
  
  fd = open(filename, O_RDWR | O_CREAT, 0666);
  if (fd == -1)
  {
    fprintf(stderr, "open file %s error: %s\n", filename, strerror(errno));
    exit(-1);
  }
  
  totRequired = 0;
  iov[0].iov_base = &myStruct;
  iov[0].iov_len = sizeof(struct point);
  totRequired += iov[0].iov_len;
  iov[1].iov_base = &x;
  iov[1].iov_len = sizeof(x);
  totRequired += iov[1].iov_len;
  iov[2].iov_base = str;
  iov[2].iov_len = strlen(str);
  totRequired += iov[2].iov_len;
  iov[3].iov_base = &a;
  iov[3].iov_len = sizeof(a);
  totRequired += iov[3].iov_len;
  
  numWritten = writev(fd, iov, 3);
  if (numWritten == -1)
  {
    fprintf(stderr, "writev error: %s\n", strerror(errno));
    exit(-1);
  }
  printf("total bytes written: %zd\n", numWritten);
 
  lseek(fd, 0, SEEK_SET); 
  numRead = readv(fd, iov, 4);
  if (numRead == -1)
  {
    fprintf(stderr, "readv error: %s\n", strerror(errno));
    exit(-1);
  }
 
  printf("total bytes requested: %zd; bytes read: %zd\n", totRequired, numRead);
  
  exit(0);
}
