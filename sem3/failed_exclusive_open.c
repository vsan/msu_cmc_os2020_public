#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[])
{
  int fd = open(argv[1], O_WRONLY);
  if (fd != -1) 
  {
    printf("[PID %ld] Error. File \"%s\" already exists!\n",(long) getpid(), argv[1]);
    close(fd);
  } 
  else 
  {
    if (errno != ENOENT) 
    {
      //что-то пошло не так
      exit(-1);
    } 
    else 
    {
      // ******************************
      // Пространство для ошибки
      printf("[PID %ld] File \"%s\" doesn't exist\n", (long) getpid(), argv[1]);
      if (argc > 2) 
      {
        sleep(5);
        printf("[PID %ld] Woke up, time to work\n", (long) getpid());
      }
      
      // ******************************
      fd = open(argv[1], O_WRONLY | O_CREAT, S_IRUSR | S_IWUSR);
      if (fd == -1)
        exit(-1);
      printf("[PID %ld] Created file \"%s\" exclusively\n", (long) getpid(), argv[1]);
      // Может быть неверно
    }
  }
  
  return 0;
}
