#include <fcntl.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


int main(int argc, char* argv[])
{
  int fd;
  char template[] = "/tmp/mkstemp_example_XXXXXX";
  fd = mkstemp(template);
  if (fd == -1)
    exit(-1);
  
  sleep(5);
  printf("Generated filename was: %s\n", template);
  unlink(template);
  // После unlink имя пропадает сразу, но сам файл будет удален после close() 
  
  // Используем файл - read(), write(), ...
  
  char buf[7];
  write(fd, "Hello!", 6);
  lseek(fd, 0, SEEK_SET);
  read(fd, &buf, 6);
  buf[6] = '\0';
  
  printf("Read back string: %s\n", buf);
  
  sleep(5);
  if (close(fd) == -1)
    exit(-1);
    
  // файл удален
  printf("Exiting successfully\n");
  
  return 0;
}
