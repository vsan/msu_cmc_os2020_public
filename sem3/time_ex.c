#include <locale.h>
#include <time.h>
#include <sys/time.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>


#define SECONDS_IN_YEAR (365.24219 * 24 * 60 * 60)

int
main(int argc, char *argv[])
{
  time_t t;
  struct tm *gmp, *locp;
  struct tm gm, loc;
  struct timeval tv;
  t = time(NULL);
  printf("Seconds since the Epoch (1 Jan 1970): %ld", (long) t);
  printf("= approximately %6.3f years\n", t / SECONDS_IN_YEAR);

  if (gettimeofday(&tv, NULL) == -1)
  {
    fprintf(stderr, "gettimeofday error: %s\n", strerror(errno));
    exit(-1);
  }
  
  printf("gettimeofday():\n %ld secs, %ld microsecs\n", (long) tv.tv_sec, (long) tv.tv_usec);

  gmp = gmtime(&t);
  if (gmp == NULL)
  {
    fprintf(stderr, "gmtime error: %s\n", strerror(errno));
    exit(-1);
  }
  
  // сохраняем локальную копию, т.к. значение *gmp может быть изменено
  // вызовами asctime() и gmtime()
  gm = *gmp; 

  printf("gmtime():\n");
  printf("  year=%d mon=%d mday=%d hour=%d min=%d sec=%d ", gm.tm_year, gm.tm_mon, gm.tm_mday, gm.tm_hour, gm.tm_min, gm.tm_sec);
  printf("wday=%d yday=%d isdst=%d\n", gm.tm_wday, gm.tm_yday, gm.tm_isdst);

  locp = localtime(&t);
  if (locp == NULL)
  {
    fprintf(stderr, "localtime error: %s\n", strerror(errno));
    exit(-1);
  }

  loc = *locp; // сохраняем локальную копию
  printf("localtime():\n");
  printf("  year=%d mon=%d mday=%d hour=%d min=%d sec=%d ", loc.tm_year, loc.tm_mon, loc.tm_mday, loc.tm_hour, loc.tm_min, loc.tm_sec);
  printf("wday=%d yday=%d isdst=%d\n\n", loc.tm_wday, loc.tm_yday, loc.tm_isdst);

  printf("asctime(gmtime()) : %s", asctime(&gm));
  printf("ctime(time()) : %s", ctime(&t));
  printf("mktime(gmtime()) : %ld secs\n", (long) mktime(&gm));
  printf("mktime(localtime()) : %ld secs\n", (long) mktime(&loc));

  printf("time difference with UTC is : %ld secs\n", (long) abs(mktime(&loc) - mktime(&gm)));

  exit(0);
}
