#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

enum { STR_SIZE = sizeof("rwxrwxrwx")};

#define SPECIAL_BITS 1
#define FILE_PERMISSIONS (S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP)
#define DIR_PERMISSIONS (S_IRWXU | S_IRWXG | S_IRWXO)
#define UMASK_SETTINGS (S_IWGRP | S_IXGRP | S_IWOTH | S_IXOTH)

char *
filePermStr(mode_t perm, int flags)
{
  static char str[STR_SIZE];
  snprintf(str, STR_SIZE, "%c%c%c%c%c%c%c%c%c",
    (perm & S_IRUSR) ? 'r' : '-', (perm & S_IWUSR) ? 'w' : '-',
    (perm & S_IXUSR) ?
      (((perm & S_ISUID) && (flags & SPECIAL_BITS)) ? 's' : 'x') :
      (((perm & S_ISUID) && (flags & SPECIAL_BITS)) ? 'S' : '-'),
    (perm & S_IRGRP) ? 'r' : '-', (perm & S_IWGRP) ? 'w' : '-',
    (perm & S_IXGRP) ?
      (((perm & S_ISGID) && (flags & SPECIAL_BITS)) ? 's' : 'x') :
      (((perm & S_ISGID) && (flags & SPECIAL_BITS)) ? 'S' : '-'),
    (perm & S_IROTH) ? 'r' : '-', (perm & S_IWOTH) ? 'w' : '-',
    (perm & S_IXOTH) ?
      (((perm & S_ISVTX) && (flags & SPECIAL_BITS)) ? 't' : 'x') :
      (((perm & S_ISVTX) && (flags & SPECIAL_BITS)) ? 'T' : '-'));
  return str;
}

int
main(int argc, char *argv[])
{
  const char* filename = "umask_test.txt";
  const char* dirname = "umask_test_dir";
  int fd;
  struct stat sb;
  mode_t u;
  umask(UMASK_SETTINGS);
  
  fd = open(filename, O_RDWR | O_CREAT | O_EXCL, FILE_PERMISSIONS);
  if (fd == -1)
  {
    fprintf(stderr, "open file %s error: %s\n", filename, strerror(errno));
    exit(-1);
  }
  
  if (mkdir(dirname, DIR_PERMISSIONS) == -1)
  { 
    fprintf(stderr, "mkdir %s error: %s\n", dirname, strerror(errno));
    exit(-1);
  }

  u = umask(0);

  // ****************************
  // file  
  if (stat(filename, &sb) == -1)
  {
    fprintf(stderr, "stat file %s error: %s\n", filename, strerror(errno));
    exit(-1);
  }

  printf("Requested file perms: %s\n", filePermStr(FILE_PERMISSIONS, 0));
  printf("Process umask: %s\n", filePermStr(u, 0));
  printf("Actual file perms: %s\n\n", filePermStr(sb.st_mode, 0));

  // ****************************
  // directory
  if (stat(dirname, &sb) == -1)
  { 
    fprintf(stderr, "stat %s error: %s\n", dirname, strerror(errno));
    exit(-1);
  }
  printf("Requested dir. perms: %s\n", filePermStr(DIR_PERMISSIONS, 0));
  printf("Process umask: %s\n", filePermStr(u, 0));
  printf("Actual dir. perms: %s\n", filePermStr(sb.st_mode, 0));
  
  
  // ****************************
  // clean
  if (unlink(filename) == -1)
  { 
    fprintf(stderr, "unlink %s error: %s\n", filename, strerror(errno));
    exit(-1);
  }
  
  if (rmdir(dirname) == -1)
  { 
    fprintf(stderr, "rmdir %s error: %s\n", dirname, strerror(errno));
    exit(-1);
  }
  
  exit(0);
}


