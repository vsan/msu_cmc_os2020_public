#include <sys/types.h>
#include <sys/stat.h>
#include <sys/sysmacros.h>
#include <time.h>
#include <fcntl.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <unistd.h>

enum { STR_SIZE = sizeof("rwxrwxrwx")};
#define SPECIAL_BITS 1
char *
filePermStr(mode_t perm, int flags)
{
  static char str[STR_SIZE];
  snprintf(str, STR_SIZE, "%c%c%c%c%c%c%c%c%c",
    (perm & S_IRUSR) ? 'r' : '-', (perm & S_IWUSR) ? 'w' : '-',
    (perm & S_IXUSR) ?
      (((perm & S_ISUID) && (flags & SPECIAL_BITS)) ? 's' : 'x') :
      (((perm & S_ISUID) && (flags & SPECIAL_BITS)) ? 'S' : '-'),
    (perm & S_IRGRP) ? 'r' : '-', (perm & S_IWGRP) ? 'w' : '-',
    (perm & S_IXGRP) ?
      (((perm & S_ISGID) && (flags & SPECIAL_BITS)) ? 's' : 'x') :
      (((perm & S_ISGID) && (flags & SPECIAL_BITS)) ? 'S' : '-'),
    (perm & S_IROTH) ? 'r' : '-', (perm & S_IWOTH) ? 'w' : '-',
    (perm & S_IXOTH) ?
      (((perm & S_ISVTX) && (flags & SPECIAL_BITS)) ? 't' : 'x') :
      (((perm & S_ISVTX) && (flags & SPECIAL_BITS)) ? 'T' : '-'));
  return str;
}

static void
displayStatInfo(const struct stat *sb)
{
  printf("File type:");
  switch (sb->st_mode & S_IFMT) 
  {
  case S_IFREG: printf("regular file\n"); break;
  case S_IFDIR: printf("directory\n"); break;
  case S_IFCHR: printf("character device\n"); break;
  case S_IFBLK: printf("block device\n"); break;
  case S_IFLNK: printf("symbolic (soft) link\n"); break;
  case S_IFIFO: printf("FIFO or pipe\n"); break;
  case S_IFSOCK: printf("socket\n"); break;
  default:
    printf("unknown file type?\n"); break;
  }
  
  printf("Device containing i-node: major=%ld minor=%ld\n", 
    (long) major(sb->st_dev), (long) minor(sb->st_dev));
  
  printf("I-node number: %ld\n", (long) sb->st_ino);

  printf("Mode: %lo (%s)\n",(unsigned long) sb->st_mode, filePermStr(sb->st_mode, 0));

  if (sb->st_mode & (S_ISUID | S_ISGID | S_ISVTX))
    printf("special bits set:%s%s%s\n",
            (sb->st_mode & S_ISUID) ? "set-UID " : "",
            (sb->st_mode & S_ISGID) ? "set-GID " : "",
            (sb->st_mode & S_ISVTX) ? "sticky " : "");
            
  printf("Number of (hard) links: %ld\n", (long) sb->st_nlink);
  
  printf("Ownership: UID=%ld GID=%ld\n", (long) sb->st_uid, (long) sb->st_gid);

  if (S_ISCHR(sb->st_mode) || S_ISBLK(sb->st_mode))
    printf("Device number (st_rdev): major=%ld; minor=%ld\n", 
      (long) major(sb->st_rdev), (long) minor(sb->st_rdev));
      
  printf("File size: %lld bytes\n", (long long) sb->st_size);
  printf("Optimal I/O block size: %ld bytes\n", (long) sb->st_blksize);
  printf("512B blocks allocated: %lld\n", (long long) sb->st_blocks);
  
  printf("Last file access: %s", ctime(&sb->st_atime));
  printf("Last file modification: %s", ctime(&sb->st_mtime));
  printf("Last status change: %s", ctime(&sb->st_ctime));
  
}
  
int
main(int argc, char *argv[])
{
  struct stat sb;
  
  if (stat(argv[1], &sb) == -1)
  {
    fprintf(stderr, "stat file %s error: %s\n", argv[1], strerror(errno));
    exit(-1);
  }
  displayStatInfo(&sb);
  exit(0);
}





    
  

