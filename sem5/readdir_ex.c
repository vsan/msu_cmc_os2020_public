#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <dirent.h>

void ls(const char *dirpath)
{
  DIR *dirp;
  struct dirent *dp;
  
  dirp = opendir(dirpath);
  if (dirp == NULL) 
  {
    fprintf(stderr, "opendir %s error: %s\n", dirpath, strerror(errno));
    return;
  }
  
  for (;;) 
  {
    errno = 0;
    dp = readdir(dirp);

    if (dp == NULL) 
      break;

    if (strcmp(dp->d_name, ".") == 0 || strcmp(dp->d_name, "..") == 0)
      continue;
      
    printf("%s/", dirpath);   
    printf("%s\n", dp->d_name);
  }
  
  if (errno != 0)
  {
    fprintf(stderr, "readdir %s error: %s\n", dirpath, strerror(errno));
    return;
  }
  
  if (closedir(dirp) == -1)
  {
    fprintf(stderr, "closedir %s error: %s\n", dirpath, strerror(errno));
  }
}

int main(int argc, char *argv[])
{
  if (argc == 1)
  {
    ls(".");
  }
  else
  {
    ls(argv[1]);
  }
  
  return 0;
}
