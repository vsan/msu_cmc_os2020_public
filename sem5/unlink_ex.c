#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>


enum {NUM_BLOCKS = 1024 * 1024, BLOCK_SIZE = 1024};

int main(int argc, char* argv[])
{
  int fd;
  int sysRet;
  char block_buf[BLOCK_SIZE];
  char cmdBuf[256];

  fd = open(argv[1], O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR);
  if(fd < 0)
  {
    fprintf(stderr, "open %s error: %s\n", argv[1], strerror(errno));
    exit(-1);
  }


  printf("Directory contents:\n");  
  snprintf(cmdBuf, 256, "ls -l `dirname %s`", argv[1]);
  sysRet = system(cmdBuf);
  if(sysRet != 0)
  {
    fprintf(stderr, "failed shell cmd: %s\n", cmdBuf);
  }
    
  if(unlink(argv[1]) < 0)
  {
    fprintf(stderr, "unlink %s error: %s\n", argv[1], strerror(errno));
    exit(-1);
  }
  
  printf("File %s unlinked\n", argv[1]);
  printf("Directory contents:\n");  
  sysRet = system(cmdBuf);
  if(sysRet != 0)
  {
    fprintf(stderr, "failed shell cmd: %s\n", cmdBuf);
  }
  
  for(int i = 0; i < NUM_BLOCKS; ++i)
  {
    ssize_t written = write(fd, block_buf, BLOCK_SIZE);
    if(written != BLOCK_SIZE)
    {
      fprintf(stderr, "write: written %zd bytes, requested %d bytes\n", written, BLOCK_SIZE);
    }
  }
  
  snprintf(cmdBuf, 256, "df -k `dirname %s`", argv[1]);
 
  sysRet = system(cmdBuf);
  if(sysRet != 0)
  {
    fprintf(stderr, "failed shell cmd: %s\n", cmdBuf);
  }
  
  if(close(fd) < 0)
  {
    fprintf(stderr, "close error: %s\n", strerror(errno));
    exit(-1);
  }
  
  printf("File descriptor closed\n");
  
  sysRet = system(cmdBuf);
  if(sysRet != 0)
  {
    fprintf(stderr, "failed shell cmd: %s\n", cmdBuf);
  }
 
  return 0;
}
