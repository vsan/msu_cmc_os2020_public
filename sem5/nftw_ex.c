#define _XOPEN_SOURCE 600

#include <fcntl.h>
#include <sys/stat.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include <ftw.h>



int myfunc(const char *pathname, const struct stat *sbuf, int type, struct FTW *ftwb)
{
  switch (sbuf->st_mode & S_IFMT) 
  {
    case S_IFREG: 
      putchar('-'); 
      break;
    case S_IFDIR: 
      putchar('d'); 
      break;
    case S_IFCHR: 
      putchar('c'); 
      break;
    case S_IFBLK: 
      putchar('b'); 
      break;
    case S_IFLNK: 
      putchar('l'); 
      break;
    case S_IFIFO: 
      putchar('p'); 
      break;
    case S_IFSOCK: 
      putchar('s'); 
      break;
    default:
      putchar('?'); break;
  }
  printf(" %s ",
    (type == FTW_D)  ? "FTW_D "  : (type == FTW_DNR) ? "FTW_DNR " :
    (type == FTW_DP) ? "FTW_DP " : (type == FTW_F)   ? "FTW_F " :
    (type == FTW_SL) ? "FTW_SL " : (type == FTW_SLN) ? "FTW_SLN " :
    (type == FTW_NS) ? "FTW_NS " : " ");

  if (type != FTW_NS)
    printf("i-node: %ld ", (long) sbuf->st_ino);
  else
    printf(" ");
    
  printf(" %*s", 4 * ftwb->level, "");
  printf("%s\n", &pathname[ftwb->base]);
  
  char cmdBuf[256];
  snprintf(cmdBuf, 256, "file %s", &pathname[ftwb->base]);
  int sysRet = system(cmdBuf);
  if(sysRet != 0)
  {
    fprintf(stderr, "failed shell cmd: %s\n", cmdBuf);
  }
  return 0;
}

int main(int argc, char *argv[])
{
  int flags = FTW_CHDIR; // | FTW_DEPTH | FTW_MOUNT | FTW_PHYS | FTW_CHDIR | FTW_ACTIONRETVAL

  if (nftw((argc > 1) ? argv[1] : ".", myfunc, 10, flags) == -1) 
  {
    fprintf(stderr, "nftw error: %s\n", strerror(errno));
    exit(-1);
  }

  return 0;
}
