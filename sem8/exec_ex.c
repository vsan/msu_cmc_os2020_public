#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>


int main(int argc, char *argv[])
{
  char *arg_array[10];
  char *env_array[] = {"NAME=execve_example", "SEMINAR=8", NULL};
  
  if(argc != 2)
  {
    fprintf(stderr, "Wrong arguments!\n");
  } 
  
  arg_array[0] = strrchr(argv[1], '/');
  if(arg_array[0] != NULL)
    arg_array[0]++;
  else
    arg_array[0] = argv[1];

  arg_array[1] = "Hello";
  arg_array[2] = "123456";
  arg_array[3] = NULL;
  
  execve(argv[1], arg_array, env_array);
  
  exit(EXIT_FAILURE);
}
