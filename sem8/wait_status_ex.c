#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

enum
{
  TOTAL_CHILDREN = 3
};


int main(int argc, char *argv[])
{

  for (int j = 0; j < TOTAL_CHILDREN; j++) 
  {
    switch (fork()) 
    {
      case -1:
        fprintf(stderr, "fork error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
      case 0:
        printf("[PID:%d] child %d\n", getpid(), j);
        if(j % 3 < 2)
        {
          _exit(j);
        }
        else
        {
          abort();
        }
      default:
        break;
    }
  }
  
  for (;;) 
  {
    int status = 0;
    pid_t child_pid = waitpid(-1, &status, 0);
    if (child_pid == -1) 
    {
      if (errno == ECHILD) 
      {
        printf("All children were waited for. Exiting...\n");
        exit(EXIT_SUCCESS);
      } 
      else 
      {
        fprintf(stderr, "wait error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
      }
    }
    if(WIFEXITED(status))
    {
      printf("child [PID:%d] exited with code %d\n", child_pid, WEXITSTATUS(status));
      if(!WEXITSTATUS(status))
      {
        printf("child [PID:%d] OK\n", child_pid);
      }
    }
    else if(WIFSIGNALED(status))
    {
      printf("child [PID:%d] terminated with signal %d\n", child_pid, WTERMSIG(status));
    }
  }
  
  exit(0);
}
