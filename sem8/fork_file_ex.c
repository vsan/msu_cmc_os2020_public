#include <sys/wait.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


int main(int argc, char *argv[])
{
  int fd, flags;
  char template[] = "/tmp/forkXXXXXX";
  
  fd = mkstemp(template);
  if (fd == -1)
  {
    fprintf(stderr, "mkstemp error : %s\n", strerror(errno));
    return 1;
  }
  
  printf("(PID:%d) File offset before fork(): %lld\n", getpid(), (long long) lseek(fd, 0, SEEK_CUR));
  flags = fcntl(fd, F_GETFL);
  if (flags == -1)
  {
    fprintf(stderr, "fcntl error : %s\n", strerror(errno));
    return 1;
  }
  
  printf("(PID:%d) O_APPEND flag before fork(): %s\n", getpid(),(flags & O_APPEND) ? "on" : "off");
  switch (fork()) 
  {
  case -1:
    fprintf(stderr, "fork error : %s\n", strerror(errno));
    return 1;
  case 0: //child
    if (lseek(fd, 1000, SEEK_SET) == -1)
    {
      fprintf(stderr, "(child) lseek error : %s\n", strerror(errno));
      return 1;
    }
    flags = fcntl(fd, F_GETFL);
    if (flags == -1)
    {
      fprintf(stderr, "(child) fcntl error : %s\n", strerror(errno));
      return 1;
    }
    flags |= O_APPEND;
    if (fcntl(fd, F_SETFL, flags) == -1)
    {
      fprintf(stderr, "(child) fcntl error : %s\n", strerror(errno));
      return 1;
    }
    printf("(PID:%d) Exiting...\n", getpid());
    _exit(EXIT_SUCCESS);
  default: //parent
    if (wait(NULL) == -1)
    {
      fprintf(stderr, "(parent) wait error : %s\n", strerror(errno));
      return 1;
    }

    printf("(PID:%d) File offset: %lld\n", getpid(), (long long) lseek(fd, 0, SEEK_CUR));
    flags = fcntl(fd, F_GETFL);
    if (flags == -1)
    {
      fprintf(stderr, "fcntl error : %s\n", strerror(errno));
      return 1;
    }
    printf("(PID:%d) O_APPEND flag in parent is: %s\n", getpid(), (flags & O_APPEND) ? "on" : "off");
    exit(EXIT_SUCCESS);
  }
  
  return 0;
}
