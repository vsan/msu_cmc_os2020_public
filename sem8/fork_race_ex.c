#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  int numChildren, j;
  pid_t childPid;
  if (argc < 2)
  {
    numChildren = 1;
  }
  else
  {
    numChildren = strtol(argv[1], NULL, 10);
  }

  setbuf(stdout, NULL); /* Make stdout unbuffered */

  for (j = 0; j < numChildren; j++) 
  {
    switch (childPid = fork()) 
    {
    case -1:
      fprintf(stderr, "fork error\n");
      return -1; 
    case 0:
      printf("%d child\n", j);
      _exit(0);
    default:
      printf("%d parent\n", j);
      wait(NULL);
      break;
    }
  }

  exit(EXIT_SUCCESS);
}
