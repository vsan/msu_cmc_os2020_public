#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

extern char **environ;
int main(int argc, char *argv[])
{
  printf("My command line arguments:\n");
  for(int i = 0; i < argc; ++i)
  {
    printf("\targv[%d] = %s\n", i, argv[i]);
  }
  
  printf("My environment variables:\n");
  for(char **p = environ; *p != NULL; p++)
  {
    printf("\t%s\n", *p);
  }
  return 0;
}
