#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>
#include <libgen.h>

enum
{
  CMD_SIZE = 256
};


int main(int argc, char *argv[])
{
  char cmd[CMD_SIZE];
  snprintf(cmd, CMD_SIZE, "ps | grep %s", basename(argv[0]));
  cmd[CMD_SIZE - 1] = '\0';
      
  pid_t child_pid;
  pid_t parent_pid = getpid();
  printf("[PID:%d] Parent process starting.\n", parent_pid);
  switch (child_pid = fork()) 
  {
    case -1:
      fprintf(stderr, "fork error: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    case 0:
      printf("[PID:%d] child: die\n", getpid());
      _exit(0);
    default:
      sleep(3);
      system(cmd);
      
      if (kill(child_pid, SIGKILL) == -1)
      {
        fprintf(stderr, "kill error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
      }
      printf("[PID:%d] Sent SIGKILL to kill the zombie (PID = %d)\n", parent_pid, child_pid);
      sleep(2);
      system(cmd);
      printf("[PID:%d] Can't kill the zombie...\n", parent_pid);
      break;
  }
  
  pid_t child_pid2 = waitpid(-1, NULL, 0);
  if (child_pid2 == -1 && errno != ECHILD) 
  {
    fprintf(stderr, "wait error: %s\n", strerror(errno));
    exit(EXIT_FAILURE);
  }
  
  printf("Only wait destroys the zombie\n");
  system(cmd);
  exit(0);
}
