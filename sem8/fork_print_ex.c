#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

const char str[] = "\nAuf Wiedersehen\n";

int main(int argc, char *argv[])
{
  //setbuf(stdout, NULL);
  printf("Hello world"); 
  
  if(write(STDOUT_FILENO, str, sizeof(str)) != sizeof(str))
  {
    fprintf(stderr, "write error: %s\n", strerror(errno));
    return 1;
  }
  
  pid_t child = fork();
  if (child == -1)
  {
    fprintf(stderr, "fork error: %s\n", strerror(errno));
    return 1;
  }
  else if (child == 0)
    _exit(0);

  _exit(0);
}
