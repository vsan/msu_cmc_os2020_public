#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <time.h>

enum
{
  TOTAL_CHILDREN = 3,
  MAX_WAIT = 5
};

int rand_n(int n)
{
  return (int)((rand() / (RAND_MAX + 1.0)) * n);
}

int main(int argc, char *argv[])
{

  srand(time(NULL));
  
  for (int j = 0; j < TOTAL_CHILDREN; j++) 
  {
    int sleep_time = rand_n(MAX_WAIT) + 1;
    switch (fork()) 
    {
      case -1:
        fprintf(stderr, "fork error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
      case 0:
        printf("[PID:%d] child %d, sleeping %d seconds\n",
               getpid(), j, sleep_time);
        sleep(sleep_time);
        _exit(EXIT_SUCCESS);
      default:
        break;
    }
  }
    ;
  pid_t child_pid;
  int num_waited = 0;
  for (;;) 
  {
    child_pid = wait(NULL);
    if (child_pid == -1) 
    {
      if (errno == ECHILD) 
      {
        printf("All children were waited for. Exiting...\n");
        exit(EXIT_SUCCESS);
      } 
      else 
      {
        fprintf(stderr, "wait error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
      }
    }
    num_waited++;
    printf("wait() returned for child with PID = %d, children waited for: %d/%d\n",
           child_pid, num_waited, TOTAL_CHILDREN);
  }
}
