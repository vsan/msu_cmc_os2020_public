#include <sys/wait.h>
#include <unistd.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <signal.h>


int main(int argc, char *argv[])
{
  if(argc != 2)
  {
    fprintf(stderr, "Wrong arguments!\n");
  } 
  
  execlp(argv[1], argv[1], "I was started with execlp", NULL);
  
  fprintf(stderr, "execlp error: %s\n", strerror(errno));
  exit(EXIT_FAILURE);
}
