#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/eventfd.h>
#include <sys/wait.h>

enum
{
  MAX_VAL = 100
};

void pingpong(uint64_t* ptr, int in, int out)
{
  int pid = getpid();
  while(1)
  {
    uint64_t res;
    read(in, &res, sizeof(res));
    
    if(*ptr > MAX_VAL)
    {
      res = 1;
      write(out, &res, sizeof(res));
      return;
    }
    
    printf("%d %llu\n", pid, (unsigned long long)*ptr);
    fflush(stdout);
    (*ptr)++;
    
    res = 1;
    write(out, &res, sizeof(res));
  }
}


int main(void)
{
  uint64_t *mem = mmap(NULL, sizeof(*mem), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
  int fd1 = eventfd(1, 0);
  int fd2 = eventfd(0, 0);
  
  *mem = 1;
  
  if(!fork())
  {
    pingpong(mem, fd1, fd2);
    _exit(1);
  }
  
  if(!fork())
  {
    pingpong(mem, fd2, fd1);
    _exit(1);
  }
  
  while(wait(NULL) > 0);
}
