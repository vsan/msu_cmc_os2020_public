#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>


union semun {
  int val;
  struct semid_ds *buf;
  unsigned short * array;
  #if defined(__linux__)
  struct seminfo * __buf;
  #endif
};

// init to 1
int init_sem_free(int sem_id, int sem_num)
{
  union semun arg;
  
  arg.val = 1;
  
  return semctl(sem_id, sem_num, SETVAL, arg);
}

// init to 0
int init_sem_occ(int sem_id, int sem_num)
{
  union semun arg;
  
  arg.val = 0;
  
  return semctl(sem_id, sem_num, SETVAL, arg);
}

// -1
int reserve_sem(int sem_id, int sem_num) 
{
  struct sembuf sem_ops;
  sem_ops.sem_num = sem_num;
  sem_ops.sem_op = -1;
  sem_ops.sem_flg = 0;
  
  while (semop(sem_id, &sem_ops, 1) == -1)
    if (errno != EINTR)
      return -1;
      
  return 0;
}

// +1
int release_sem(int sem_id, int sem_num) 
{
  struct sembuf sem_ops;
  sem_ops.sem_num = sem_num;
  sem_ops.sem_op = 1;
  sem_ops.sem_flg = 0;
  return semop(sem_id, &sem_ops, 1);
}

enum
{
  SHM_KEY = 0xDEAD,
  SEM_KEY = 0xBEEF,
  PERMS   = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP,
  WRITE_SEM = 0,
  READ_SEM  = 1,
  BUF_SIZE  = 1024
};

struct shm_segment
{
  int  nBytes;
  char buf[BUF_SIZE];
};


void error_exit(const char *msg)
{
  fprintf(stderr, "%s error : %s\n", msg, strerror(errno));
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{
  /* получаем id семафора и разд. памяти */ 
  int semid = semget(SEM_KEY, 0, 0);
  if (semid == -1)
    error_exit("semget");
  int shmid = shmget(SHM_KEY, 0, 0);
  if (shmid == -1)
    error_exit("shmget");
    
  /* присоединяем разд. память для чтения */
  struct shm_segment *shmp = shmat(shmid, NULL, SHM_RDONLY);
  if (shmp == (void *) -1)
    error_exit("shmat");

  /* в цикле данные из разд. памяти направляем на stdout */
  int iter, bytes;
  for (iter = 0, bytes = 0; ; iter++) 
  {
    /* занимаем семафор читателя */
    if (reserve_sem(semid, READ_SEM) == -1)
      error_exit("reserve_sem");
      
    /* проверяем число байт, записанных в разд. память,
       если 0, значит писатель закончил */
    if (shmp->nBytes == 0)
      break;  
    bytes += shmp->nBytes;
    
    /* читаем из разд. памяти и направляем на stdout */
    if (write(STDOUT_FILENO, shmp->buf, shmp->nBytes) != shmp->nBytes)
      error_exit("write");
    if (release_sem(semid, WRITE_SEM) == -1)
      error_exit("release_sem");
  }

  /* отсоединяем разд. память, это не удаление */
  if (shmdt(shmp) == -1)
    error_exit("shmdt");

  /* освобождаем семафор писателя, чтобы он понял,
     что читатель закончил работу */
  if (release_sem(semid, WRITE_SEM) == -1)
    error_exit("release_sem");
    
  fprintf(stderr, "Received %d bytes (%d iterations)\n", bytes, iter);
  exit(EXIT_SUCCESS);
}
    
    
    
    
