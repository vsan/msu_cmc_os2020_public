#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>


int intArg(const char* str)
{
  return strtol(str, NULL, 10);
}


int main(int argc, char *argv[])
{
  int pipe_fd[2];
  
  if(argc < 2)
  {
    fprintf(stderr, "Wrong arguments\n");
    exit(1);
  }

  setbuf(stdout, NULL);

  printf("[PID:%d] Parent process started\n", getpid());

  if(pipe(pipe_fd) == -1)
  {
   fprintf(stderr, "pipe error: %s\n", strerror(errno));
  }

  for(int i = 1; i < argc; ++i)
  {
    switch(fork())
    {
    case -1: 
      fprintf(stderr, "fork error: %s\n", strerror(errno));
      exit(1);
    case 0:
      if(close(pipe_fd[0]) == -1)
        fprintf(stderr, "child: close pipe error: %s\n", strerror(errno));

      /* do some work parent needs to wait to complete */
      sleep(intArg(argv[i]));
      printf("[PID:%d] child %d finished, closing pipe\n", getpid(), i);

      if(close(pipe_fd[1]) == -1)
        fprintf(stderr, "child: close pipe error: %s\n", strerror(errno));

      /* do some other work */
      sleep(2);
      _exit(0);

    default:
      break;
    }
  }
  
  if(close(pipe_fd[1]) == -1)
    fprintf(stderr, "parent: close pipe error: %s\n", strerror(errno));


  /* do some work */
  int sync;
  if(read(pipe_fd[0], &sync, 1) != 0)
  {
    fprintf(stderr, "something wrong, no EOF from PIPE\n");
  }

  fprintf(stdout, "[PID:%d] Parent synced with children\n", getpid());

  /* do some other work */

  while(1)
  {
    int status = 0;
    pid_t child = waitpid(-1, &status, 0);
    if(child == -1 && errno == ECHILD)
    {
      printf("All children were waited for. Exiting...\n");
      break;
    }  
    printf("child %d status: %s\n", (int)child,
         (WIFEXITED(status) && !WEXITSTATUS(status)) ? "success" : "fail");
  }

  exit(0);
}
