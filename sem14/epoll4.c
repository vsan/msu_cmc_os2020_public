#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/signalfd.h>
#include <unistd.h>
#include <sys/epoll.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>

enum
{
  MAX_EVENTS = 16
};

int main(void)
{
  sigset_t ss;
  sigemptyset(&ss);
  sigaddset(&ss, SIGINT);
  sigprocmask(SIG_BLOCK, &ss, NULL);
  
  int sig_fd = signalfd(-1, &ss, SFD_NONBLOCK);
  
  printf("PID: %d\n", getpid());
  
  /* регистрируем sig_fd для обработки через механизм epoll */
  int efd = epoll_create1(0);
  struct epoll_event e1 = {.events = EPOLLIN, .data.u32 = 0xdead};
  epoll_ctl(efd, EPOLL_CTL_ADD, sig_fd, &e1);
  
  fcntl(STDIN_FILENO, F_SETFL, fcntl(STDIN_FILENO, F_GETFL) | O_NONBLOCK);
  struct epoll_event e2 = {.events = EPOLLIN, .data.u32 = 0xbeef};
  epoll_ctl(efd, EPOLL_CTL_ADD, STDIN_FILENO, &e2);  
  
  while(1)
  {
    struct epoll_event events[MAX_EVENTS];
    int res = epoll_wait(efd, events, MAX_EVENTS, 600);
    switch(res)
    {
    case -1:
      fprintf(stderr, "epoll_wait error: %s\n", strerror(errno));
      exit(EXIT_FAILURE);
    case 0:
      printf("epoll_wait timeout\n");
      break;
      
    default:
      for(int i = 0; i < res; ++i)
      {
        struct epoll_event *e = &events[i];
        if(e->data.u32 == 0xdead)
        {
          struct signalfd_siginfo sinfo;
          while(read(sig_fd, &sinfo, sizeof(sinfo)) == sizeof(sinfo))
          {
            printf("received signal: %d from [PID:%d][UID:%d]", sinfo.ssi_signo, sinfo.ssi_pid, sinfo.ssi_uid);
          }
        }
        else if(e->data.u32 == 0xbeef)
        {
          char buf[64];
          while(read(STDIN_FILENO, buf, sizeof(buf)) > 0)
          {
            printf("stdin >> %s\n", buf);
          } 
        }
      }
    }
    
    
  }
}
