#include <sys/types.h>
#include <sys/sem.h>
#include <sys/shm.h>
#include <sys/stat.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>



union semun {
  int val;
  struct semid_ds *buf;
  unsigned short * array;
  #if defined(__linux__)
  struct seminfo * __buf;
  #endif
};

// init to 1
int init_sem_free(int sem_id, int sem_num)
{
  union semun arg;
  
  arg.val = 1;
  
  return semctl(sem_id, sem_num, SETVAL, arg);
}

// init to 0
int init_sem_occ(int sem_id, int sem_num)
{
  union semun arg;
  
  arg.val = 0;
  
  return semctl(sem_id, sem_num, SETVAL, arg);
}

// -1
int reserve_sem(int sem_id, int sem_num) 
{
  struct sembuf sem_ops;
  sem_ops.sem_num = sem_num;
  sem_ops.sem_op = -1;
  sem_ops.sem_flg = 0;
  
  while (semop(sem_id, &sem_ops, 1) == -1)
    if (errno != EINTR)
      return -1;
      
  return 0;
}

// +1
int release_sem(int sem_id, int sem_num) 
{
  struct sembuf sem_ops;
  sem_ops.sem_num = sem_num;
  sem_ops.sem_op = 1;
  sem_ops.sem_flg = 0;
  return semop(sem_id, &sem_ops, 1);
}

enum
{
  SHM_KEY = 0xDEAD,
  SEM_KEY = 0xBEEF,
  PERMS   = S_IRUSR | S_IWUSR | S_IRGRP | S_IWGRP,
  WRITE_SEM = 0,
  READ_SEM  = 1,
  BUF_SIZE  = 1024
};

struct shm_segment
{
  int  nBytes;
  char buf[BUF_SIZE];
};

//   fprintf(stderr, "msgget error : %s\n", strerror(errno));

void error_exit(const char *msg)
{
  fprintf(stderr, "%s error : %s\n", msg, strerror(errno));
  exit(EXIT_FAILURE);
}

int main(int argc, char *argv[])
{  

  /* создаем два семафора для читателя и писателя */ 
  int semid = semget(SEM_KEY, 2, IPC_CREAT | PERMS);
  if (semid == -1)
    error_exit("semget");
    
  if (init_sem_free(semid, WRITE_SEM) == -1)
    error_exit("init_sem_free");
  if (init_sem_occ(semid, READ_SEM) == -1)
    error_exit("init_sem_occ");

  /* создаем сегмент разделяемой памяти */
  int shmid = shmget(SHM_KEY, sizeof(struct shm_segment), IPC_CREAT |PERMS);
  
  if (shmid == -1)
    error_exit("shmget");

  struct shm_segment *shmp = shmat(shmid, NULL, 0);
  if (shmp == (void *) -1)
    error_exit("shmat");

  /* цикл записывает данные со стандартного ввода в разделяемую
     память */
  int bytes, iter;
  for (iter = 0, bytes = 0; ; iter++, bytes += shmp->nBytes) 
  {
    /* занимаем семафор */
    if (reserve_sem(semid, WRITE_SEM) == -1)
      error_exit("reserve_sem");
    
    /* читаем данные с stdin в разделяемую память */
    shmp->nBytes = read(STDIN_FILENO, shmp->buf, BUF_SIZE);
    if (shmp->nBytes == -1)
      error_exit("read");
      
    /* освобождаем семафор */
    if (release_sem(semid, READ_SEM) == -1)
      error_exit("release_sem");
    
    /* данных больше нет, читатель узнает об этом
     из поля shmp->nBytes */
    if (shmp->nBytes == 0)
      break;
  }

  /* занимаем семафор ещё раз, чтобы удостовериться,
     что читатель закончил работу и теперь можно удалить 
     IPC объекты */
  if (reserve_sem(semid, WRITE_SEM) == -1)
    error_exit("reserve_sem");
    
  /* удаляем семафор и разделяемую память */
  union semun tmp;
  if (semctl(semid, 0, IPC_RMID, tmp) == -1)
    error_exit("semctl");
  if (shmdt(shmp) == -1)
    error_exit("shmdt");
  if (shmctl(shmid, IPC_RMID, 0) == -1)
   error_exit("shmctl");
  
  fprintf(stdout, "Sent %d bytes (%d iterations)\n", bytes, iter);
  exit(EXIT_SUCCESS);
}
    
    
    
    
    
