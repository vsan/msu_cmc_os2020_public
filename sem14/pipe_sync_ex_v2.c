#include <stdio.h>
#include <stdint.h>
#include <unistd.h>
#include <sys/mman.h>
#include <sys/eventfd.h>
#include <sys/wait.h>

enum
{
  MAX_VAL = 100
};

void pingpong(uint64_t* ptr, int in, int out)
{
  int pid = getpid();
  while(1)
  {
    int sync;
    read(in, &sync, sizeof(sync));
    
    if(*ptr > MAX_VAL)
    {
      sync = 1;
      write(out, &sync, sizeof(sync));
      return;
    }
    
    printf("%d %llu\n", pid, (unsigned long long)*ptr);
    fflush(stdout);
    (*ptr)++;
    
    sync = 1;
    write(out, &sync, sizeof(sync));
  }
}


int main(void)
{
  uint64_t *mem = mmap(NULL, sizeof(*mem), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
  
  int pipe12[2];
  int pipe21[2];
  
  pipe(pipe12);
  pipe(pipe21);

  *mem = 1;
  
  int sync = 1;
  write(pipe12[1], &sync, sizeof(sync));
  if(!fork())
  {
    close(pipe12[1]);
    close(pipe21[0]);
    pingpong(mem, pipe12[0], pipe21[1]);
    _exit(1);
  }
  
  if(!fork())
  {
    close(pipe12[0]);
    close(pipe21[1]);
    pingpong(mem, pipe21[0], pipe12[1]);
    _exit(1);
  }
  
  close(pipe12[0]);
  close(pipe12[0]);
  close(pipe21[0]);
  close(pipe21[1]);
  
  while(wait(NULL) > 0);
}
