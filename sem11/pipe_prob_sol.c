#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

void pingpong(int in, int out, int maxval, const char* msg)
{
  pid_t mypid = getpid();
  while(1)
  {
    int x;
    if(read(in, &x, sizeof(x)) != sizeof(x))
      break;
    
    dprintf(STDOUT_FILENO, "[PID:%d] %s %d\n", mypid, msg, x);
    
    if(x == maxval)
      break;
      
    ++x;
    write(out, &x, sizeof(x));
  }
}

int main(int argc, char* argv[])
{
  int maxval;
  sscanf(argv[1], "%d", &maxval);

  int pipe12[2];
  int pipe21[2];
  
  pipe(pipe12);  
  pipe(pipe21);
 
  int init = 1;
  write(pipe21[1], &init, sizeof(init));
  int pid1 = fork();
  if(!pid1)
  {
    close(pipe12[0]);
    close(pipe21[1]);
    pingpong(pipe21[0], pipe12[1], maxval, "ping");
    _exit(0);
  } 
  int pid2 = fork();
  if(!pid2)
  {
    close(pipe12[1]);
    close(pipe21[0]);
    pingpong(pipe12[0], pipe21[1], maxval, "pong");
    _exit(0);
  } 
  
  close(pipe12[0]);
  close(pipe21[1]);
  close(pipe12[1]);
  close(pipe21[0]);
  
  while(wait(NULL) != -1) {};
 
  exit(EXIT_SUCCESS);  
}
