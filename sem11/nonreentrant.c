#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>
#include <crypt.h>


// gcc -Wall -Werror -o nonreentrant nonreentrant.c -lcrypt

static char *str2;
static int handled = 0;

static void handler(int sig)
{
  crypt(str2, "xx");
  handled++;
}

int main(int argc, char* argv[])
{
  if (argc != 3)
    fprintf(stderr, "wrong arguments!\n");
    
  str2 = argv[2];
  char *cr1 = strdup(crypt(argv[1], "xx"));
  
  if (cr1 == NULL)
    fprintf(stderr, "strdup error\n");
    
  struct sigaction sa;   
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = handler;
  if (sigaction(SIGINT, &sa, NULL) == -1)
    fprintf(stderr, "sigaction error: %s\n", strerror(errno));
    
  /*
    Вызываем crypt() для argv[1]. При прерывании обработчиком сигнала
    статическая память, возвращаемая crypt() будет перезаписана результатом
    вызова crypt для argv[2] в обработчике сигнала.
    И сравнение строк вернет несоответствие.
  */
  int fail = 0;
  for (int i = 1; ; ++i) 
  {
    if (strcmp(crypt(argv[1], "xx"), cr1) != 0) 
    {
      fail++;
      printf("[%d] mismatch! (fail=%d handled=%d)\n", i, fail, handled);
    }
  }
}
