#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <sys/wait.h>
#include <limits.h>
#include <fcntl.h>
#include <string.h>
#include <errno.h>

int main(int argc, char* argv[])
{
  char path[PATH_MAX];
  int rand_fd = open("/dev/urandom", O_RDONLY);
  unsigned long long val;
  read(rand_fd, &val, sizeof(val));
  snprintf(path, sizeof(path), "/tmp/fifo%llx", val);
  close(rand_fd);
  
  
  if(mkfifo(path, 0600) < 0)
  {
    fprintf(stderr, "mkfifo: %s\n", strerror(errno));
    exit(1);
  }
  
  int pid1 = fork();
  if(!pid1)
  {
    FILE *f = fopen(path, "r");
    int x, y;
    fscanf(f, "%d%d", &x, &y);
    printf("%d\n", x + y);
    _exit(0);
  } 
  
 
  FILE *out = fopen(path, "w");
  fprintf(out, "%s %s ", argv[1], argv[2]);
  fflush(out);
  
  fclose(out);
  wait(NULL); 
  
  unlink(path);
   
  exit(EXIT_SUCCESS);  
}
