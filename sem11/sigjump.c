#define _GNU_SOURCE // strsignal()
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <setjmp.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>

static volatile sig_atomic_t can_jump = 0;
//static sigjmp_buf senv;
static jmp_buf env;

int printSignalMask(FILE *out_file, const char *msg) 
{
  sigset_t current_mask;
  
  if (msg != NULL)
    fprintf(out_file, "%s", msg);
    
  if (sigprocmask(SIG_BLOCK, NULL, &current_mask) == -1)
    return -1;
    
  int cnt = 0;
  for (int sig = 1; sig < NSIG; sig++) 
  {
    if (sigismember(&current_mask, sig)) 
    {
      cnt++;
      fprintf(out_file, "\t\t%d (%s)\n", sig, strsignal(sig));
    }
  }
  
  if (cnt == 0)
    fprintf(out_file, "\t\tempty signal set\n");
    
  return 0;
}

static void handler(int sig)
{
  /* printf, strsignal - UNSAFE */
  printf("Received signal %d (%s), signal mask is:\n", sig, strsignal(sig));
  printSignalMask(stdout, NULL);
  if (!can_jump) 
  {
    printf("flag not set, return from signal handler\n");
    return;
  }

//  siglongjmp(senv, 1);
  longjmp(env, 1);
}


int main(int argc, char* argv[])
{
  printSignalMask(stdout, "Signal mask:\n");
  
  struct sigaction sa;
  sigemptyset(&sa.sa_mask);
  sa.sa_flags = 0;
  sa.sa_handler = handler;
  
  if (sigaction(SIGINT, &sa, NULL) == -1)
    fprintf(stderr, "sigaction error: %s\n", strerror(errno));

  printf("Set jump\n");
//  if (sigsetjmp(senv, 1) == 0) 
  if (setjmp(env) == 0)
    can_jump = 1; //after sigsetjmp
  else //after siglongjmp
    printSignalMask(stdout, "After jump:\n" );

  for (;;)
    pause();
}
