#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <sys/types.h>
#include <sys/wait.h>

int main(int argc, char* argv[])
{
  int pipeFD[2];
  
  pipe(pipeFD);
  
  int pid1 = fork();
  if(!pid1)
  {
    close(pipeFD[1]);
    FILE *f = fdopen(pipeFD[0], "r");
    int x, y;
    fscanf(f, "%d%d", &x, &y);
    printf("%d\n", x + y);
    _exit(0);
  } 
  
  close(pipeFD[0]);
  
  //FILE *out = fdopen(pipeFD[1], "w");
  //fprintf(out, "%s %s", argv[1], argv[2]);
  //fflush(out);
  //fclose(out);
  
  dprintf(pipeFD[1], "%s %s", argv[1], argv[2]);
  close(pipeFD[1]);
  wait(NULL);
  
//  fclose(out);
  exit(EXIT_SUCCESS);  
}
