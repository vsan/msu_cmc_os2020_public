#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>


static /*volatile*/ sig_atomic_t flag;
static void sigHandler(int sig)
{
  flag = 1; 
}

int main(int argc, char *argv[])
{
  printf("PID = %d\n", getpid());
  sigset_t s1, s2;
  sigemptyset(&s1);
 // sigaddset(&s1, SIGINT);
  sigaddset(&s1, SIGTERM);
  sigprocmask(SIG_BLOCK, &s1, &s2);
  
  struct sigaction sa;
  sa.sa_mask = s1;
  sa.sa_flags = SA_RESTART;
  sa.sa_handler = sigHandler;
  
  if (sigaction(SIGINT, &sa, NULL) == -1)
    fprintf(stderr, "sigaction error: %s\n", strerror(errno));
  
  for (int j = 0; j < 5; j++) 
  {
  //  if(!flag)
    while(!flag)
    {
      //sigsuspend(&s2);
    }
    flag = 0;
    printf("SIGINT #%d\n", j);
//    sleep(3);
  }
  

  exit(0);
}
