#define _GNU_SOURCE
#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <errno.h>


int main(int argc, char *argv[])
{
  sigset_t s1, s2;
  sigfillset(&s1);
  sigprocmask(SIG_SETMASK, &s1, &s2);
  printf("[PID:%d] Signals blocked, sleeping\n", getpid());
  
  sleep(3);
  
  printf("[PID:%d] Woke up\n", getpid());
  
  for (;;) 
  {
    siginfo_t si;
    int sig = sigwaitinfo(&s1, &si);
    if(sig == -1)
    {
      fprintf(stderr, "sigwaitinfo error: %s\n", strerror(errno));
    }
    
    if(sig == SIGINT || sig == SIGTERM)
      exit(0);
      
    printf("Received signal: %d (%s)\n", sig, strsignal(sig));
    
    printf("\tfrom pid=%d, uid=%d\n", si.si_pid, si.si_uid);
    
  }

  exit(0);
}
