#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <signal.h>
#include <string.h>
#include <sys/signalfd.h>
#include <errno.h>


// signalfd is Linux specific

int main(int argc, char *argv[])
{
  printf("PID = %d\n", getpid());
  sigset_t s1, s2;
  sigemptyset(&s1);
  sigaddset(&s1, SIGINT);
  sigprocmask(SIG_BLOCK, &s1, &s2);
  
  int sigfd = signalfd(-1, &s1, 0);
  
  for (int j = 0; j < 5; j++) 
  {
    struct signalfd_siginfo ss;
    read(sigfd, &ss, sizeof(ss));
    printf("SIGINT(%d) #%d sent from [PID:%d][UID:%d]\n",
           ss.ssi_code, j, ss.ssi_pid, ss.ssi_uid);

  }

  exit(0);
}
