#include <signal.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>

int receiver_pid, num, max_num;
int fd[2];
int status;

void handler(int s)
{
  int old_err = errno;
  if(num < max_num)
  {
    read(fd[0], &num, sizeof(int));
    printf("%d:%d\n", getpid(), num);
    num++;
    write(fd[1], &num, sizeof(int));
    kill(receiver_pid, SIGUSR1);
  }
  else
  {
    if(receiver_pid == getppid())
    {
      printf("Child process exiting ...\n");
      close(fd[1]); close(fd[0]);
      exit(0);
    }
    else
    {
      kill(receiver_pid, SIGUSR1);
    }
  }
  errno = old_err;
}

int main(void)
{
  scanf("%d", &max_num);
  pipe(fd);
  
  struct sigaction sa;
  sa.sa_flags = SA_RESTART;
  sa.sa_handler = handler;
  sigemptyset(&sa.sa_mask);
  
  if (sigaction(SIGUSR1, &sa, NULL) == -1)
    fprintf(stderr, "sigaction error: %s\n", strerror(errno));
    
  num = 0;
  if((receiver_pid = fork()))
  {
    wait(&status);
    close(fd[1]); close(fd[0]);
    exit(0);
  }
  else
  {
    receiver_pid = getppid();
    write(fd[1], &num, sizeof(int));
    kill(receiver_pid, SIGUSR1);
    while(1)
      pause();
  }
}
