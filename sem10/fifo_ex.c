#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>


enum 
{
  BUF_SIZE = 10
};

int main(int argc, char *argv[])
{
  int fifo_in, fifo_out;
  char buffer[BUF_SIZE];
  ssize_t nRead;

  if(argc != 2)
  {
    fprintf(stderr, "Wrong arguments\n");
    exit(1);
  }

  if(mkfifo("fifo_file", 0666) == -1)
  {
   fprintf(stderr, "mkfifo error: %s\n", strerror(errno));
  }

  switch(fork())
  {
  case -1: 
    fprintf(stderr, "fork error: %s\n", strerror(errno));
    exit(1);
  case 0:
    fifo_in = open("fifo_file", O_WRONLY);
    if(fifo_in == -1)
      fprintf(stderr, "open error: %s\n", strerror(errno));
      
    if(write(fifo_in, argv[1], strlen(argv[1])) != strlen(argv[1]))
      fprintf(stderr, "partial write\n");
      
    if(close(fifo_in) == -1)
      fprintf(stderr, "child close pipe read error: %s\n", strerror(errno));

    _exit(0);

  default:
   fifo_out = open("fifo_file", O_RDONLY);
   if(fifo_out == -1)
      fprintf(stderr, "open error: %s\n", strerror(errno));
      
   while(1)
    {
      nRead = read(fifo_out, buffer, BUF_SIZE);
      if(nRead == -1)
        fprintf(stderr, "read from pipe error: %s\n", strerror(errno));

      if(nRead == 0) // EOF
        break; 
      
      fprintf(stdout, "Message read from pipe: "); fflush(stdout);
      if(write(STDOUT_FILENO, buffer, nRead) != nRead)
        fprintf(stderr, "partial write\n");
        
      if(!write(STDOUT_FILENO, "\n", 1))
        fprintf(stderr, "final write error: %s\n", strerror(errno));
    }

    if(close(fifo_out) == -1)
      fprintf(stderr, "close pipe write error: %s\n", strerror(errno));

    wait(NULL);
    exit(0);
  }
  return 1;
}
