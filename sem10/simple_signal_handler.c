#include <signal.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>


static void sigHandler(int sig)
{
  printf("Received SIGINT\n"); // UNSAFE
}

int main(int argc, char *argv[])
{
  int j;
  if (signal(SIGINT, sigHandler) == SIG_ERR)
  {
    fprintf(stderr, "signal error");
    exit(1);
  }

  for (j = 0; j < 5; j++) 
  {
    printf("%d\n", j);
    sleep(3);
  }

  exit(0);
}
