#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>

int main(int argc, char *argv[])
{
  int pipe_fd[2];

  if(pipe(pipe_fd) == -1)
  {
   fprintf(stderr, "pipe error: %s\n", strerror(errno));
  }

  pid_t child_out, child_in;

  switch((child_out = fork()))
  {
  case -1: 
    fprintf(stderr, "fork error: %s\n", strerror(errno));
    exit(1);
  case 0:
    if(close(pipe_fd[0]) == -1)
      fprintf(stderr, "close pipe fd error: %s\n", strerror(errno));

    dup2(pipe_fd[1], STDOUT_FILENO);
  
    if(close(pipe_fd[1]) == -1)
      fprintf(stderr, "close pipe fd error: %s\n", strerror(errno));

    execlp("ls", "ls", NULL);

    fprintf(stderr, "execlp ls error: %s\n", strerror(errno));
    _exit(2);

  default:
    break;
  }


  switch((child_in = fork()))
  {
  case -1: 
    fprintf(stderr, "fork error: %s\n", strerror(errno));
    exit(1);
  case 0:
    if(close(pipe_fd[1]) == -1)
      fprintf(stderr, "close pipe fd error: %s\n", strerror(errno));
     
    dup2(pipe_fd[0], STDIN_FILENO);
 
    if(close(pipe_fd[0]) == -1)
      fprintf(stderr, "close pipe fd error: %s\n", strerror(errno));

    fprintf(stderr, "\"ls | wc -l\" command output is\n");
    execlp("wc", "wc", "-l", NULL);
 
    fprintf(stderr, "execlp wc error: %s\n", strerror(errno));
    _exit(2);
 
  default:
    break;
  }

  if(close(pipe_fd[0]) == -1)
    fprintf(stderr, "close pipe fd error: %s\n", strerror(errno));
  if(close(pipe_fd[1]) == -1)
    fprintf(stderr, "close pipe fd error: %s\n", strerror(errno));

  int status = 0;
  waitpid(child_out, &status, 0);
  printf("[CHILD:%d] ls: %s\n", child_out, 
         (WIFEXITED(status) && !WEXITSTATUS(status)) ? "success" : "fail");

  waitpid(child_in, &status, 0);
  printf("[CHILD:%d] wc: %s\n", child_in,
         (WIFEXITED(status) && !WEXITSTATUS(status)) ? "success" : "fail");

  return 1;
}
