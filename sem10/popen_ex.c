#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>
#include <limits.h>
#include <ctype.h>


static char* cmd_format =  "/bin/ls -d %s 2> /dev/null";
enum
{
  PATTERN_SIZE = 64,
  CMD_BUF_SIZE = sizeof(cmd_format) + PATTERN_SIZE
};

int main(int argc, char *argv[])
{
  char pattern[PATTERN_SIZE];
  char popen_cmd[CMD_BUF_SIZE];
  char pathname[PATH_MAX];

  while(1) 
  {
    printf("Enter search pattern: "); fflush(stdout);
  
    if (fgets(pattern, PATTERN_SIZE, stdin) == NULL)
      break;

    int len = strlen(pattern);
    if (len <= 1)
      continue;
    
    if (pattern[len - 1] == '\n')
      pattern[len - 1] = '\0';
    
    _Bool is_bad = 0;
    int i = 0;
    for (; i < len && !is_bad; i++)
    {
      if (!isalnum((unsigned char) pattern[i]) &&
           strchr("_*?[^-].", pattern[i]) == NULL)
      {
        is_bad = 1;
      }
    }
    if(is_bad) 
    {
      printf("Not allowed in pattern: %c\n", pattern[i - 1]);
      continue;
    }
    
    snprintf(popen_cmd, CMD_BUF_SIZE, cmd_format, pattern);
    popen_cmd[CMD_BUF_SIZE - 1] = '\0';
    
    FILE *fp = popen(popen_cmd, "r");
    if (fp == NULL) 
    {
      fprintf(stderr, "popen() failed\n");
      continue;
    }
    
    int found = 0;
    while(fgets(pathname, PATH_MAX, fp) != NULL) 
    {
      printf("\t%s", pathname);
      found++;
    }

    int status = pclose(fp);
    if(status == -1)
      fprintf(stderr, "pclose error: %s\n", strerror(errno));

    printf("Total found files: %d\n", found);  

  }
  
  printf("\n");
  return 0;
}
