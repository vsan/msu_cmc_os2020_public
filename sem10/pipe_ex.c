#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <fcntl.h>
#include <stdlib.h>


enum 
{
  BUF_SIZE = 10
};

int main(int argc, char *argv[])
{
  int pipe_fd[2];
  char buffer[BUF_SIZE];
  ssize_t nRead;

  if(argc != 2)
  {
    fprintf(stderr, "Wrong arguments\n");
    exit(1);
  }

  if(pipe(pipe_fd) == -1)
  {
   fprintf(stderr, "pipe error: %s\n", strerror(errno));
  }

  switch(fork())
  {
  case -1: 
    fprintf(stderr, "fork error: %s\n", strerror(errno));
    exit(1);
  case 0:
    if(close(pipe_fd[1]) == -1)
      fprintf(stderr, "close pipe fd error: %s\n", strerror(errno));

    while(1)
    {
      nRead = read(pipe_fd[0], buffer, BUF_SIZE);
      if(nRead == -1)
        fprintf(stderr, "read from pipe error: %s\n", strerror(errno));

      if(nRead == 0) // EOF
        break; 
      
      fprintf(stdout, "Message read from pipe: "); fflush(stdout);
      if(write(STDOUT_FILENO, buffer, nRead) != nRead)
        fprintf(stderr, "partial write\n");
    }

    if(!write(STDOUT_FILENO, "\n", 1))
      fprintf(stderr, "final write error: %s\n", strerror(errno));
  
    if(close(pipe_fd[0]) == -1)
      fprintf(stderr, "child close pipe read error: %s\n", strerror(errno));

    _exit(0);

  default:
    if(close(pipe_fd[0]) == -1)
      fprintf(stderr, "close pipe read error: %s\n", strerror(errno));
      
    if(write(pipe_fd[1], argv[1], strlen(argv[1])) != strlen(argv[1]))
      fprintf(stderr, "partial write\n");

    if(close(pipe_fd[1]) == -1)
      fprintf(stderr, "close pipe write error: %s\n", strerror(errno));

    wait(NULL);
    exit(0);
  }
  return 1;
}
