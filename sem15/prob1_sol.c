#include <stdio.h>
#include <stdlib.h>
#include <sys/ipc.h>
#include <sys/sem.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <math.h>

enum
{
  DATA_SIZE = 10
};

#define CHECK_RESULT(f) 												 \
{																								 \
    int __result = (f);													 \
    if (__result < 0)                            \
    {                                            \
        fprintf(stderr, "error at line %d: %s\n", __LINE__, strerror(errno)); \
        exit(1);                                 \
    }																						 \
}

void work(int semid, int id, float *ptr, int pipefd)
{
  float *p_data = malloc(DATA_SIZE * sizeof(*ptr));
  if(!p_data)
  {
    fprintf(stderr, "malloc: %s\n", strerror(errno));
    _exit(1);
  }

  for(int i = 0; i < DATA_SIZE; ++i)
  {
    if(id == 1)
      p_data[i] = ptr[i] * ptr[i];
    else
      p_data[i] = cosf(ptr[i]);
  }

  struct sembuf op1[] = { { .sem_num = 0, .sem_op  = -1, .sem_flg = 0 }};
  CHECK_RESULT(semop(semid, op1, sizeof(op1) / sizeof(op1[0])))

  struct sembuf op2[] = {{ .sem_num = 0, .sem_op  = 0, .sem_flg = 0 }};
  CHECK_RESULT(semop(semid, op2, sizeof(op2) / sizeof(op2[0])))

  if(id == 1)
  {
    for (int i = 0; i < DATA_SIZE; ++i)
    {
      ptr[i] = p_data[i];
    }
  }
  else
  {
    if(write(pipefd, p_data, sizeof(p_data[0]) * DATA_SIZE) != sizeof(p_data[0]) * DATA_SIZE)
    {
      fprintf(stderr, "write: %s\n", strerror(errno));
      _exit(1);
    }
  }

  if(id == 1)
  {
    struct sembuf op3[] = { { .sem_num = 1, .sem_op  = 1, .sem_flg = 0 }};
    CHECK_RESULT(semop(semid, op3, sizeof(op3) / sizeof(op3[0])))

    if(read(pipefd, p_data, sizeof(p_data[0]) * DATA_SIZE) != sizeof(p_data[0]) * DATA_SIZE)
    {
      fprintf(stderr, "read: %s\n", strerror(errno));
      _exit(1);
    }
    float sum = 0;
    for (int i = 0; i < DATA_SIZE; ++i)
    {
      sum += p_data[i];
    }
    printf("1: sum = %f\n", sum);
  }
  else
  {
    struct sembuf op3[] = { { .sem_num = 1, .sem_op  = -1, .sem_flg = 0 }};
    CHECK_RESULT(semop(semid, op3, sizeof(op3) / sizeof(op3[0])))

    float sum = 0;
    for (int i = 0; i < DATA_SIZE; ++i)
    {
      sum += ptr[i];
    }
    printf("2: sum = %f\n", sum);
  }

  _exit(0);
}

int main(void)
{
  float data[] = {1, 2, 3, 4, 5, 6, 7, 8, 9, 10};
  int semid = semget(IPC_PRIVATE, 2, 0600 | IPC_CREAT | IPC_EXCL);
  if (semid < 0)
  {
    fprintf(stderr, "semget: %s\n", strerror(errno));
    exit(1);
  }

  int p21[2];
  if(pipe(p21) != 0)
  {
    fprintf(stderr, "pipe: %s\n", strerror(errno));
    exit(1);
  }
  semctl(semid, 0, SETVAL, 2);
  semctl(semid, 1, SETVAL, 0);

  float *ptr = mmap(NULL, sizeof(data), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
  for(int i = 0; i < DATA_SIZE; ++i)
  {
    ptr[i] = data[i];
  }

  if (!fork())
  {
    close(p21[1]);
    work(semid, 1, ptr, p21[0]);
  }

  if (!fork())
  {
    close(p21[0]);
    work(semid, 2, ptr, p21[1]);
  }

  close(p21[0]);
  close(p21[1]);
  while(wait(NULL) != -1);

  semctl(semid, 0, IPC_RMID);
}
