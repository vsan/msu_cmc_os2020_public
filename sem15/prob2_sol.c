#include <sys/sem.h>
#include <sys/shm.h>
#include <unistd.h>
#include <sys/wait.h>
#include <sys/mman.h>
#include <stdlib.h>
#include <stdio.h>
#include <errno.h>
#include <string.h>


enum
{
  N = 10,
  M = 16
};

union semun {
  int val;
  struct semid_ds *buf;
  unsigned short * array;
#if defined(__linux__)
  struct seminfo * __buf;
#endif
};

void initDB(int id, int* data)
{
  for(int i = 0; i < M; ++i)
  {
    data[i] = id * 100 + i;
  }
}

void work(int sem, int id, int* ptr, const int* data)
{
  while(1)
  {
    struct sembuf op1[] = {{.sem_num = id, .sem_op  = -1, .sem_flg = 0}};
    int sem_test = 0;
    if((sem_test = semop(sem, op1, sizeof(op1) / sizeof(op1[0]))) < 0)
    {
      printf("sem_test = %d\n", sem_test);
      if(sem_test == EIDRM)
        _exit(0);
      else
      {
        fprintf(stderr, "semop error: %s\n", strerror(errno));
      }
    }

    int db_id = *ptr;
    if (db_id >= M)
    {
      printf("[PID:%d, ID:%d] exit\n", id, getpid());
      _exit(0);
    }
    *ptr = data[db_id];

    struct sembuf op2[] = {{.sem_num = 0, .sem_op  = 1, .sem_flg = 0}};
    if((sem_test = semop(sem, op2, sizeof(op2) / sizeof(op2[0]))) < 0)
    {
      printf("sem_test = %d\n", sem_test);
      if(sem_test == EIDRM)
        _exit(0);
      else
      {
        fprintf(stderr, "semop error: %s\n", strerror(errno));
      }
    }
  }
}

int main(void)
{
  int sem = semget(IPC_PRIVATE, N + 1, 0600 | IPC_CREAT | IPC_EXCL);
  unsigned short init[N + 1] = {0};
  union semun arg;
  arg.array = init;
  semctl(sem, 0, SETALL, arg);

  int *ptr = mmap(NULL, sizeof(*ptr), PROT_READ | PROT_WRITE, MAP_ANONYMOUS | MAP_SHARED, -1, 0);
  for(int i = 1; i <= N; ++i)
  {
    if(!fork())
    {
      int data[M];
      initDB(i, data);
      work(sem, i, ptr, data);
    }
  }

  int id = -1;
  while(scanf("%d", &id) != EOF)
  {
    int proc = id / M;
    int db   = id % M;
    if(proc >= N)
    {
      printf("Incorrect id\n");
    }
    else
    {
      *ptr = db;
      struct sembuf op[] = {{.sem_num = proc + 1, .sem_op  = 1, .sem_flg = 0}};
      if(semop(sem, op, sizeof(op) / sizeof(op[0])) < 0)
      {
        fprintf(stderr, "parent semop error: %s\n", strerror(errno));
      }
      op[0].sem_num = 0;
      op[0].sem_op  = -1;
      op[0].sem_flg = 0;
      if(semop(sem, op, sizeof(op) / sizeof(op[0])) < 0)
      {
        fprintf(stderr, "parent semop error: %s\n", strerror(errno));
      }

      printf("[id = %d] value = %d, stored at %d\n", id, *ptr, proc + 1);
    }
  }
  *ptr = M;
  for(int i = 1; i <= N; ++i)
  {
    semctl(sem, i, SETVAL, 1);
  }

  semctl(sem, 0, IPC_RMID);
  while(wait(NULL) != -1);

  return 0;
}
