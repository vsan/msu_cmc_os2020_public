#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

enum
{
    TEXT_SIZE = 1024
};

struct mbuf {
    long mtype;
    char mtext[TEXT_SIZE];
};

int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        fprintf(stderr, "not enough arguments\n");
        return 1;        
    }

    int msqid = strtol(argv[1], NULL, 10);
    int type = strtol(argv[2], NULL, 10);

    struct mbuf msg;
    int flags = 0;
    int msgLen = msgrcv(msqid, &msg, TEXT_SIZE, type, flags);
    if(msgLen == -1)
    {
        fprintf(stderr, "msgrcv error: %s\n", strerror(errno));
        return 1;
    }
    
    printf("Received message: type=%ld, length=%d, text:\n", msg.mtype, msgLen);
    printf("\t %s\n", msg.mtext);

    exit(EXIT_SUCCESS);
}
