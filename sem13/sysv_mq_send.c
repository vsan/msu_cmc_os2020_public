#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

enum
{
    TEXT_SIZE = 1024
};

struct mbuf {
    long mtype;
    char mtext[TEXT_SIZE];
};

int main(int argc, char *argv[])
{
    if (argc < 4)
    {
        printf("Creating SysV message queue...\n");

        int id = msgget(IPC_PRIVATE, 0666);
        if(id == -1)
        {
            fprintf(stderr, "msgget error : %s\n", strerror(errno));
            return 1;
        }
        else
        {
            printf("Id: %d\n", id);
            return 0;
        }
    }

    int msqid = strtol(argv[1], NULL, 10);
    struct mbuf msg;
    msg.mtype = strtol(argv[2], NULL, 10);

    
    int msgLen = strlen(argv[3]) + 1;
    memcpy(msg.mtext, argv[3], msgLen > TEXT_SIZE ? TEXT_SIZE : msgLen);

    int flags = 0;
    if (msgsnd(msqid, &msg, msgLen, flags) == -1)
    {
        fprintf(stderr, "msgsnd error : %s\n", strerror(errno));
    }

    exit(EXIT_SUCCESS);
}
