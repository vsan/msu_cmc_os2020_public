#include <sys/stat.h>
#include <mqueue.h>
#include <fcntl.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
    if (argc < 3)
    {
        printf("Creating POSIX message queue...\n");
        int flags = O_CREAT | O_RDWR;
        mqd_t mqd = mq_open(argv[1], flags, 0666, NULL);
        if(mqd == (mqd_t)-1)
        {
            fprintf(stderr, "mq_open error : %s\n", strerror(errno));
            return 1;
        }
        else
        {
            printf("Created POSIX message queue with name %s\n", argv[1]);
            return 0;
        }
    }

    mqd_t mqd = mq_open(argv[1], O_WRONLY);
    if(mqd == (mqd_t)-1)
    {
        fprintf(stderr, "mq_open error : %s\n", strerror(errno));
        return 1;
    }

    int priority = 0;
    int res = mq_send(mqd, argv[2], strlen(argv[2]), priority); 
    if(res == -1)
    {
        fprintf(stderr, "mq_send error : %s\n", strerror(errno));
        return 1;
    }
 
    exit(EXIT_SUCCESS);
}
