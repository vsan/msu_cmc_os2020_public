#include <sys/types.h>
#include <sys/msg.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>


int main(int argc, char *argv[])
{
  struct msginfo msginfo;
  
  int maxind = msgctl(0, MSG_INFO, (struct msqid_ds *) &msginfo);
  if (maxind == -1)
    fprintf(stderr, "msgctl with MSG_INFO\n");
    
  printf("maxind: %d\n\n", maxind);
  printf("index\tid\tkey\t\tmessages\n");

  for (int ind = 0; ind <= maxind; ind++) 
  {
    struct msqid_ds ds;
    int msqid = msgctl(ind, MSG_STAT, &ds);
    if (msqid == -1) 
    {
      if (errno != EINVAL && errno != EACCES)
       fprintf(stderr, "msgctl with MSG_STAT\n");
      continue;
    }
    
    printf("%d\t%d\t0x%08lx\t%ld\n", ind, msqid, (unsigned long)ds.msg_perm.__key, (long) ds.msg_qnum);
  }
  exit(EXIT_SUCCESS);
}
