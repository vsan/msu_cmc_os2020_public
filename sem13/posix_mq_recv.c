#include <sys/stat.h>
#include <mqueue.h>
#include <unistd.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>

enum
{
    MAX_MSG = 50,
    TEXT_SIZE = 1024
};


int main(int argc, char *argv[])
{
    if (argc < 2)
    {        
        fprintf(stderr, "not enough arguments\n");
        return 1;
    }

    mqd_t mqd = mq_open(argv[1], O_RDONLY);
    if(mqd == (mqd_t)-1)
    {
        fprintf(stderr, "mq_open error : %s\n", strerror(errno));
        return 1;
    }

    struct mq_attr attr;
    if(mq_getattr(mqd, &attr) == -1)
    {
        fprintf(stderr, "mq_getattr error : %s\n", strerror(errno));
        return 1;
    }

    void *buffer = malloc(attr.mq_msgsize);
    if(buffer == NULL)
    {
        fprintf(stderr, "malloc error : %s\n", strerror(errno));
        return 1;
    }
    
    unsigned int priority = 0;
    ssize_t nRead = mq_receive(mqd, buffer, attr.mq_msgsize, &priority);
    if(nRead == -1)
    {
        fprintf(stderr, "mq_receive error : %s\n", strerror(errno));
        return 1;
    }

    printf("Read %zd bytes, priority = %u, text:\n", nRead, priority);
    if(write(STDOUT_FILENO, buffer, nRead) != nRead)
    {
        fprintf(stderr, "write error : %s\n", strerror(errno));
        return 1;
    }
    write(STDOUT_FILENO, "\n", 1);

    exit(EXIT_SUCCESS);
}
