#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>
#include <fcntl.h>

int main(int argc, char **argv)
{
  if(argc < 5)
  {
    fprintf(stderr, "not enough arguments\n");
    exit(1);
  }

  pid_t pid = fork();
  if(pid == 0)
  {
    int fd_in = open(argv[2], O_RDONLY, 0);
    if(fd_in < 0)
    {
      fprintf(stderr, "open %s error : %s\n", argv[2], strerror(errno));
      exit(1);
    }
    dup2(fd_in, STDIN_FILENO); close(fd_in);
    
    printf("stdin redirect to %s have been set up\n", argv[2]);
    
    int fd_out = open(argv[3], O_WRONLY | O_CREAT | O_TRUNC, 0666);
    if(fd_out < 0)
    {
      fprintf(stderr, "open %s error : %s\n", argv[3], strerror(errno));
      exit(1);
    }
    int my_out = dup(STDOUT_FILENO);
    dup2(fd_out, STDOUT_FILENO); close(fd_out);
    fcntl(my_out, F_SETFD, fcntl(my_out, F_GETFD) | FD_CLOEXEC);
    
    dprintf(my_out, "stdout redirect to %s have been set up\n", argv[3]);
    
    int fd_outerr = open(argv[4], O_WRONLY | O_CREAT | O_APPEND, 0666);
    if(fd_outerr < 0)
    {
      fprintf(stderr, "open %s error : %s\n", argv[4], strerror(errno));
      exit(1);
    }
    int my_err = fcntl(STDERR_FILENO, F_DUPFD_CLOEXEC);
    dup2(fd_outerr, STDERR_FILENO); close(fd_outerr);
  
    dprintf(my_out, "stderr redirect to %s have been set up\n", argv[4]);
  
    execlp(argv[1], argv[1], NULL);
    dprintf(my_err, "execlp %s error\n", argv[1]);
    _exit(1);
  }
  else if(pid < 0)
  {
    fprintf(stderr, "fork error : %s\n", strerror(errno));
    exit(1);
  }

  int status = 0;
  waitpid(pid, &status, 0);
  printf("%s\n", 
         (WIFEXITED(status) && !WEXITSTATUS(status)) ? "success" : "fail");

  return 0;
}
