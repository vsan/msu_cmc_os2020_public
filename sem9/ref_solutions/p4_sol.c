#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


int main(int argc, char **argv)
{
  for(int i = 1; i < argc; ++i)
  {
    int status;
    pid_t pid;
    if((pid = fork()) > 0)
    {
      pid = wait(&status);
      printf("process %s finished with status %d\n", argv[i], WEXITSTATUS(status));
      printf("----------\n");
    }
    else if(pid == 0)
    {
      execlp(argv[i], argv[i], NULL);
      _exit(1);
    }
    else
    {
      fprintf(stderr, "fork %d error : %s\n", i, strerror(errno));
      exit(1);
    }
  }

  return 0;
}
