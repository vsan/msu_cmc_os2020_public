#include <unistd.h>
#include <sys/wait.h>
#include <sys/types.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <stdlib.h>


int main(void)
{
  pid_t child = 0;
  pid_t grandchild = 0;
  switch((child = fork()))
  {
    case -1:
      fprintf(stderr, "fork error: %s\n", strerror(errno));
      exit(1);
    case 0:
       grandchild = fork();
       if(grandchild == -1)
       {
         fprintf(stderr, "fork error in child: %s\n", strerror(errno));
         _exit(1);
       }
       else if(grandchild == 0)
       {
         printf("[PID:%d, PPID:%d] check 1\n", getpid(), getppid());
         fflush(stdout);
         sleep(1);
         printf("[PID:%d, PPID:%d] check 2\n", getpid(), getppid());
         fflush(stdout);
         sleep(5); 
         printf("[PID:%d, PPID:%d] check 3\n", getpid(), getppid());
         fflush(stdout);
         _exit(0);
       }
       else
       {
         printf("Child exiting...\n"); fflush(stdout);
         _exit(0);
       }
    default:
      sleep(3);
      printf("Grandparent waiting for child...\n");
      fflush(stdout);
      wait(NULL);
      sleep(3);
      printf("Grandparent exiting...\n");
      fflush(stdout);
      break;
  }
  exit(0);
}
