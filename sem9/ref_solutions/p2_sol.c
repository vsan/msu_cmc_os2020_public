#include <sys/wait.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>
#include <stdio.h>
#include <stdint.h>
#include <inttypes.h>

enum
{
 N_NUMBERS = 4
};

int main(void)
{   
  int pid = 0;
  uint32_t data[N_NUMBERS] = {};
  for (int i = 0; i < N_NUMBERS; i++) 
  {
    if(scanf("%"SCNd32, &data[i]) < 0)
    {
      fprintf(stderr, "scanf failed at %d-th number\n", i);
      exit(1);
    }
  }
  for (int i = 0; i < N_NUMBERS; i++) 
  {
    switch((pid = fork()))
    {
      case 0:
        fprintf(stdout, "[PPID:%d,PID:%d]: x = %"PRId32" x^2 = %"PRId32"\n", getppid(), getpid(), data[i], data[i] * data[i]);
        fflush(stdout);
        _exit(0);
      case -1:
        fprintf(stderr, "fork error: %s\n", strerror(errno));
        exit(1);
      default:
        break;
    }
  }

  while (wait(NULL) != -1);
  return 0;
}
