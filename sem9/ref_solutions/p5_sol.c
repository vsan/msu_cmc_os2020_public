#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>


int main(int argc, char **argv)
{
  for(int i = 1; i < argc; ++i)
  {
    pid_t pid;
    if((pid = fork()) == 0)
    {
      execlp(argv[i], argv[i], NULL);
      _exit(1);
    }
    else if(pid < 0)
    {
      fprintf(stderr, "fork %d error : %s\n", i, strerror(errno));
      exit(1);
    }
  }

  while(1)
  {
    int status = 0;
    pid_t child_pid = waitpid(-1, &status, 0);
    if (child_pid == -1) 
    {
      if (errno == ECHILD) 
      {
        printf("All children were waited for. Exiting...\n");
        exit(EXIT_SUCCESS);
      } 
      else 
      {
        fprintf(stderr, "wait error: %s\n", strerror(errno));
        exit(EXIT_FAILURE);
      }
    }
    if(WIFEXITED(status))
    {
      printf("child [PID:%d] exited with code %d\n", child_pid, WEXITSTATUS(status));
      if(!WEXITSTATUS(status))
      {
        printf("child [PID:%d] OK\n", child_pid);
      }
    }
    else if(WIFSIGNALED(status))
    {
      printf("child [PID:%d] terminated with signal %d\n", child_pid, WTERMSIG(status));
    }
  }

  return 0;
}
