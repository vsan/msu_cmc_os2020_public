#include <sys/types.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdio.h>
#include <stdlib.h>
#include <errno.h>
#include <string.h>

int main(int argc, char **argv)
{
  if(argc < 2)
    exit(1);

  pid_t pid = fork();
  if(pid == 0)
  {
    execlp("/bin/sh", "sh", "-c", argv[1], NULL);
    _exit(1);
  }
  else if(pid < 0)
  {
    fprintf(stderr, "fork error : %s\n", strerror(errno));
    exit(1);
  }

  int status = 0;
  waitpid(pid, &status, 0);
  printf("%s\n", 
         (WIFEXITED(status) && !WEXITSTATUS(status)) ? "success" : "fail");

  return 0;
}
