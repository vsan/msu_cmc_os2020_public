#include <stdio.h>
#include <unistd.h>

int main(int argc, char * argv[])
{

  printf("Numbers from 0 to 100:\n");
  for(int i = 0; i <= 100; ++i)  
  {
    printf("%d ", i);
  }
  fflush(stdout);
  sleep(15);

  putchar('\n');

  return 0;
}
