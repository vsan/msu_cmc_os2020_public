#include <stdio.h>


int main(int argc, char* argv[])
{
  if(argc < 2)
  {
    return 1;
  }
  
  for(int i = 0; i < argc; ++i)
  {
    printf("arg[%d] = %s\n", i, argv[i]);
  }

  return 0;
}


