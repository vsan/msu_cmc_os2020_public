#include <stdio.h>
#include <limits.h>

int main(int argc, char* argv[])
{
  printf("Enter two integers\n");
  int a = 2;
  int b = 5;
  if(scanf("%d%d", &a, &b) != 2)
    printf("error! will use default values\n");

  printf("%d * %d = %d, 100%%\n", a, b, a * b);
  printf("%lx\n", 3735928559); 
  printf("%09o\n", 511); 
  printf("%llu\n", LLONG_MAX); 
  printf("%.2f\n", 3.141592653589); 
  printf("%.4g\n", 2.2 * 100000000000000); 

  return 0;
}
