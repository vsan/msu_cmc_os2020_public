#include <stdio.h>
#include <math.h>

typedef double (*fp)(double);

double numint(fp func, double a, double b, int n)
{
  double res = 0.0;
  double h = (b - a) / n;

  a += h / 2;  
  while(a <= b)
  {
    res += func(a);
    a += h;
  }
  
  return res * h;
}

double f(double x)
{
  return x;
}

int main(int argc, char* argv[])
{

  printf("I(sin(x), 0.0, 3.14, 10) = %f\n", numint(sin, 0.0, 3.14, 10));
  printf("I(y = x, 0.0, 1.0, 10) = %f\n", numint(f, 0.0, 1.0, 10));

  return 0;
}
