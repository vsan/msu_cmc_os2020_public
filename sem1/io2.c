#include <stdio.h>
#include <errno.h>
#include <string.h>
#include <stdlib.h>

extern char** environ;

int main(int argc, char* argv[])
{
  printf("program arguments:\n");  
  for(int i = 0; i < argc; ++i)
  {
    printf("arg[%d] = %s\n", i, argv[i]);
  }

  if(argc > 1)
  {
    char* var = getenv(argv[1]);
    if(var)
    {
      printf("%s = %s\n", argv[1], var);
    }
    else
    {
      printf("environment variable \"%s\" not found\n", argv[1]);
    }
  }

  if(argc > 2)
  {
    FILE *myfile = fopen(argv[2], "r");
    if(!myfile)
    {
      fprintf(stderr, "error opening %s : %s\n", argv[2], strerror(errno));
      return 1;
    }
    else
    {
    //char 
      printf("Contents of %s:\n", argv[2]);
      int c;
      while((c = fgetc(myfile)) != EOF)
      {
        //putchar(c);
        fputc(c, stdout);
      }
    }
  }

  return 0;
}


