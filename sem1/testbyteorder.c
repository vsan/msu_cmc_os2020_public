/* from wiki:
https://ru.wikipedia.org/wiki/%D0%9F%D0%BE%D1%80%D1%8F%D0%B4%D0%BE%D0%BA_%D0%B1%D0%B0%D0%B9%D1%82%D0%BE%D0%B2
*/

#include <stdio.h>
#include <stdint.h>

int main(void)
{
  uint16_t x = 1; /* 0x0001 */
  printf("%s\n", *((uint8_t *) &x) == 0 ? "big-endian" : "little-endian");
  return 0;
}
