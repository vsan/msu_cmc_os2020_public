#include <stdio.h>

int main()
{
	int a, b;
	scanf("%d%d", &a, &b);
	int c;
  if (__builtin_add_overflow(a, b, &c)) 
  {
    fprintf(stderr, "overflow!\n");
  } 
  else 
  {
	  printf("a + b = %d\n", c);
  }
}

